class Dev_logs:
    def __init__(self, input_file, logs_format, frames_count, rssi, dev_types, manu_ids, unique_SN_count, frames_from_SN ):
        self.name = ''
        if logs_format == 'k':
            self.name = 'drive-by_dongle'
        elif logs_format == 'd':
            self.name = 'device_dongle'
        elif logs_format == 's':
            self.name = 's2lp'
        elif logs_format == 't':
            self.name = 'telit'
        elif logs_format == 'tr':
            self.name = 'telit_rssi'
        self.input_file = input_file
        self.frames_count = frames_count
        self.rssi = rssi
        self.dev_types = dev_types
        self.manu_ids = manu_ids
        self.unique_SN_count = unique_SN_count
        self.frames_from_SN = frames_from_SN


