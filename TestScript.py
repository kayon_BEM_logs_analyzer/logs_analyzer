#!/bin/python3

import sys
import getopt
import re
from TelitRSSI import *
from WMBUS import *
from MaciejProtocol import *
from Dev_logs import *
from collections import Counter
import xlsxwriter
import time

test = False
# test = True

def logger(argv):

    iterations = 0
    input_file = []
    output_file = ''
    logs_format = []
    results = []
#    print("args: " + str(argv))
    try:
        opts, args = getopt.getopt(argv, "i:f:o:", ["ifile=", "format="])
    except getopt.GetoptError:
        print('analyzer.py -i1 -i2 -i3 <inputfile> -f1 -f2 -f3 <format: k-kayon, d-device, s-s2lp, t-telit> -o <outputfile>')
        sys.exit(2)
    for opt,arg in opts:
#        print("Opt: " +str(opt) + " arg: " + str(arg))
        if opt == '-h':
            #print('analyzer.py -i1 -i2 -i3 <inputfile> -f1 -f2 -f3 <format: k-kayon, d-device, s-s2lp, t-telit> -o <outputfile>')
            print('TestScript.py -i <inputfile> -f <format: k-kayon, d-device, s-s2lp, t-telit> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_file.append(arg)
            iterations = iterations+1
        elif opt in ("-f", "--format"):
            logs_format.append(arg)
        #elif opt in ("-i2", "--i2file"):
        #    input_file.append(arg)
        #    iterations = iterations+1
        #    print("Input2 argument OK")
        #elif opt in ("-f2", "--format2"):
        #    logs_format.append(arg)
        #    print("Format2 argument OK")
        #elif opt in ("-i3", "--i3file"):
        #    input_file.append(arg)
        #    iterations = iterations+1
        #    print("Input3 argument OK")
        #elif opt in ("-f3", "--format3"):
        #    logs_format.append(arg)
        #    print("Format3 argument OK")
        elif opt in ("-o", "--ofile"):
            output_file = arg


    if test:
        iterations = 3
        input_file.append('C:\KAYON\kayon_projekty\BEM-IoT\Drive-by_Dongle\logs\BLOKI_MORENA_02_02_2018\BLOKI_MORENA_02_02_2018\DBD_16_33')
        logs_format.append('k')
        input_file.append('C:\KAYON\kayon_projekty\BEM-IoT\Drive-by_Dongle\logs\BLOKI_MORENA_02_02_2018\BLOKI_MORENA_02_02_2018\S2LP_16_51')
        logs_format.append('s')
        input_file.append('C:\KAYON\kayon_projekty\BEM-IoT\Drive-by_Dongle\logs\BLOKI_MORENA_02_02_2018\BLOKI_MORENA_02_02_2018\TELIT_16_42')
        logs_format.append('tr')
        output_file = 'C:\KAYON\kayon_projekty\BEM-IoT\Drive-by_Dongle\logs\BLOKI_MORENA_02_02_2018\BLOKI_MORENA_02_02_2018\DBD_S2LP_TEL_COMP_16_33-16_42'
    #print("Iterations: " + str(iterations))
    for i in range(iterations):
        results.append(analyze_logs(input_file[i], logs_format[i]))

    #generate_results(output_file, results, iterations)


def analyze_logs(input_file, logs_format):

    hex_logs = None
    frames_start = []

#    print('Input file path = ', input_file)
    if logs_format == 't':
 #       print("telit")
        hex_logs = readHexLogsFromFile(input_file)
        skip = 0
        for i, byte in enumerate(hex_logs):
            if i < skip:
                continue
            else:
                if byte == 0x44:
                    frames_start.append(i-1)
                    length = hex_logs[i-1]
                    skip = length+i
        frames_start.append(len(hex_logs))
    elif logs_format == 'tr':
 #       print("telit rssi")
        hex_logs = readHexLogsFromFile(input_file)
        skip = 0
        for i, byte in enumerate(hex_logs):
            if i < skip:
                continue
            else:
                if byte == 0x44:
                    frames_start.append(i - 1)
                    length = hex_logs[i - 1]
                    skip = length + i + 1
        frames_start.append(len(hex_logs))
    elif logs_format == 'd':
        print("device")
    elif logs_format == 'k':
 #       print("kayon")
        hex_logs = readHexLogsFromFile(input_file)
        skip = 0
        start_byte_counter = 0
        for i,byte in enumerate(hex_logs):
            if i < skip:
                continue
            else:
                if byte == 0x55:
                    start_byte_counter = start_byte_counter+1
                else:
                    start_byte_counter = 0
                if start_byte_counter == 4:
                    frames_start.append(i-3)
                    length = hex_logs[i+2]
                    skip = length + 2 + i
    elif logs_format == 's':
  #      print("s2lp")
        hex_logs = readHexLogsFromFile(input_file)
        skip = 0
        start_byte_counter = 0
        for i,byte in enumerate(hex_logs):
            if i < skip:
                continue
            else:
                if byte == 0x55:
                    start_byte_counter = start_byte_counter+1
                else:
                    start_byte_counter = 0
                if start_byte_counter == 4:
                    frames_start.append(i-3)
                    length = hex_logs[i+2]
                    skip = length + 2 + i
    else:
        print("default")

    frame_begin_and_end = [(frames_start[i], frames_start[i+1]) for i, elem in enumerate(frames_start[:-1])]

    frames = [hex_logs[elem[0]:elem[1]] for elem in frame_begin_and_end]
    if logs_format == 'k' or logs_format == 's':
        dd_frames = [createDDFrame(frame) for frame in frames]
    elif logs_format == 't':
        dd_frames = [createDDTelitFrame(frame) for frame in frames]
    elif logs_format == 'tr':
        dd_frames = [createDDTelitRssiFrame(frame) for frame in frames]

    frames_count = len(dd_frames)
    device_type_distr = device_type_distribution(dd_frames)
    if logs_format != 't':
        rssi_distr = RSSI_distribution(dd_frames)
    manu_id_distr = manufacturers_id_distribution(dd_frames)
    unique_SN_count = unique_SN_distribution(dd_frames)
    frames_from_SN = frames_from_SN_distribution(dd_frames)
    if logs_format != 't':
        dev_logs = Dev_logs(input_file, logs_format, frames_count, rssi_distr, device_type_distr, manu_id_distr,
                            unique_SN_count, frames_from_SN)
    else:
        dev_logs = Dev_logs(input_file, logs_format, frames_count, None, device_type_distr, manu_id_distr,
                            unique_SN_count, frames_from_SN)
    print("Frames count: " + str(frames_count) + "\nUnique_devs: " + str(unique_SN_count))
    return dev_logs


def generate_results(file_name, logs_results, iterations):

    chart_rssi = []
    chart_dev_type = []
    chart_manu_id = []
    chart_frames_from_SN = []

    workbook = xlsxwriter.Workbook(file_name + '.xlsx')
    for dev_i in range(iterations):
        sheet_name = logs_results[dev_i].name + str(dev_i)
        worksheet = workbook.add_worksheet(sheet_name)
        worksheet.set_column('A:A', 16)

        format1 = workbook.add_format()
        format1.set_bg_color('silver')
        format1.set_bold(True)

        worksheet.write(0, 0, 'Device', format1)
        worksheet.write(0, 1, logs_results[dev_i].name, format1)
        worksheet.write(1, 0, 'Frames count')
        worksheet.write(1, 1, logs_results[dev_i].frames_count)

        worksheet.write(2, 0, 'Unique devices', format1)
        worksheet.write(2, 1, logs_results[dev_i].unique_SN_count)

        if logs_results[dev_i].rssi is not None:
            worksheet.write(3, 0, 'RSSI:', format1)
            worksheet.write(4, 0, 'Count:')
            for i, rssi_elem in enumerate(logs_results[dev_i].rssi):
                worksheet.write(3, i+1, rssi_elem[0], format1)
                worksheet.write(4, i+1, rssi_elem[1])
            chart_rssi.append(workbook.add_chart({'type': 'column'}))
            chart_rssi[dev_i].set_x_axis({
                'name': 'RSSI'
            })
            chart_rssi[dev_i].add_series({
                'name': logs_results[dev_i].name,
                'categories': [sheet_name, 3, 1, 3, len(logs_results[dev_i].rssi)],#'='+sheet_name+'!$B$3:$AF$3', #[sheet_name, 2, 1, 2, len(rssi)],
                'values': [sheet_name, 4, 1, 4, len(logs_results[dev_i].rssi)],#'='+sheet_name+'!$B$4:$AF$4' #[sheet_name, 3, 1, 3, len(rssi)],
            })
            worksheet.insert_chart('C10', chart_rssi[dev_i])

        worksheet.write(25, 0, 'Device type:', format1)
        worksheet.write(26, 0, 'Count:')
        for i, dev_type in enumerate(logs_results[dev_i].dev_types):
            worksheet.write(25, i+1, dev_type[0], format1)
            worksheet.write(26, i+1, dev_type[1])
        chart_dev_type.append(workbook.add_chart({'type': 'column'}))
        chart_dev_type[dev_i].set_x_axis({
            'name': 'Device_type'
        })
        chart_dev_type[dev_i].add_series({
            'name': logs_results[dev_i].name,
            'categories': [sheet_name, 25, 1, 25, len(logs_results[dev_i].dev_types)],
            'values': [sheet_name, 26, 1, 26, len(logs_results[dev_i].dev_types)],
        })
        worksheet.insert_chart('C33', chart_dev_type[dev_i])

        worksheet.write(50, 0, 'Manufacturers ID:', format1)
        worksheet.write(51, 0, 'Count:')
        for i, manu_id in enumerate(logs_results[dev_i].manu_ids):
            worksheet.write(50, i+1, manu_id[0], format1)
            worksheet.write(51, i+1, manu_id[1])
        chart_manu_id.append(workbook.add_chart({'type': 'column'}))
        chart_manu_id[dev_i].set_x_axis({
            'name': 'Manufactuters ID'
        })
        chart_manu_id[dev_i].add_series({
            'name': logs_results[dev_i].name,
            'categories': [sheet_name, 50, 1, 50, len(logs_results[dev_i].manu_ids)],
            'values': [sheet_name, 51, 1, 51, len(logs_results[dev_i].manu_ids)],
        })
        worksheet.insert_chart('C58', chart_manu_id[dev_i])

        worksheet.write(75, 0, 'SN:', format1)
        worksheet.write(76, 0, 'Count:')
        for i, SN in enumerate(logs_results[dev_i].frames_from_SN):
            worksheet.write(75, i+1, SN[0], format1)
            worksheet.write(76, i+1, SN[1])
        chart_frames_from_SN.append(workbook.add_chart({'type': 'column'}))
        chart_frames_from_SN[dev_i].set_x_axis({'name': 'Frames from SN'})
        chart_frames_from_SN[dev_i].add_series({
            'name': logs_results[dev_i].name,
            'categories': [sheet_name, 75, 1, 75, len(logs_results[dev_i].frames_from_SN)],
            'values': [sheet_name, 76, 1, 76, len(logs_results[dev_i].frames_from_SN)],
        })
        worksheet.insert_chart('C83', chart_frames_from_SN[dev_i])

    sheet_name = 'comp'
    worksheet = workbook.add_worksheet(sheet_name)
    worksheet.set_column('A:A', 16)

    format1 = workbook.add_format()
    format1.set_bg_color('silver')
    format1.set_bold(True)
#   chart_rssi_comp = workbook.add_chart({'type': 'column'})
#    chart_rssi_comp.set_x_axis({
#        'name': 'RSSI'
#    })
#    chart_dev_type_comp = workbook.add_chart({'type': 'column'})
#    chart_dev_type_comp.set_x_axis({
#        'name': 'Device_type'
#    })
#    chart_manu_id_comp = workbook.add_chart({'type': 'column'})
#    chart_manu_id_comp.set_x_axis({
#        'name': 'Manufactuters ID'
#    })
#
#    chart_frames_from_SN_comp = workbook.add_chart({'type': 'column'})
#    chart_frames_from_SN_comp.set_x_axis({
#        'name': 'Frames from SN'
#    })
    rssi_cmp = []
    dev_type_cmp = []
    manu_id_cmp = []
    frames_from_SN_cmp = []
    for dev_i in range(iterations):
        if logs_results[dev_i].rssi is not None:
            for rssi_elem in logs_results[dev_i].rssi:
                if rssi_elem[0] not in rssi_cmp:
                    rssi_cmp.append(rssi_elem[0])
        for dev_type_elem in logs_results[dev_i].dev_types:
            if dev_type_elem[0] not in dev_type_cmp:
                dev_type_cmp.append(dev_type_elem[0])
        for manu_id_elem in logs_results[dev_i].manu_ids:
            if manu_id_elem[0] not in manu_id_cmp:
                manu_id_cmp.append(manu_id_elem[0])
        for frames_from_SN in logs_results[dev_i].frames_from_SN:
            if frames_from_SN[0] not in frames_from_SN_cmp:
                frames_from_SN_cmp.append(frames_from_SN[0])

    rssi_cmp.sort()

    for dev_i in range(iterations):

        worksheet.write(0, 0, 'Device', format1)
        worksheet.write(0, 1+dev_i, logs_results[dev_i].name, format1)
        worksheet.write(1, 0, 'Frames count')
        worksheet.write(1, 1+dev_i, logs_results[dev_i].frames_count)

        worksheet.write(2, 0, 'Unique devices')
        worksheet.write(2, 1+dev_i, logs_results[dev_i].unique_SN_count)

        if logs_results[dev_i].rssi is not None:
            worksheet.write(3, 0, 'RSSI', format1)
            worksheet.write(4+dev_i, 0, logs_results[dev_i].name)
            for i, rssi in enumerate(rssi_cmp):
                worksheet.write(3, i+1, rssi, format1)
                for j, rssi_el in enumerate(logs_results[dev_i].rssi):
                    if rssi_el[0] == rssi:
                        worksheet.write(4+dev_i, i+1, rssi_el[1])
        #    chart_rssi_comp.add_series({
        #        'name': logs_results[dev_i].name,
        #        'categories': [sheet_name, 3 + dev_i, 1, 3 + dev_i, len(rssi_cmp)],
        #        # '='+sheet_name+'!$B$3:$AF$3', #[sheet_name, 2, 1, 2, len(rssi)],
        #        'values': [sheet_name, 4 + dev_i, 1, 4 + dev_i, len(rssi_cmp)],
        #        # '='+sheet_name+'!$B$4:$AF$4' #[sheet_name, 3, 1, 3, len(rssi)],
        #    })

        worksheet.write(25, 0, 'Device type', format1)
        worksheet.write(26+dev_i, 0, logs_results[dev_i].name)
        for i, dev_type in enumerate(dev_type_cmp):
            worksheet.write(25, i + 1, dev_type, format1)
            for j, dev_type_el in enumerate(logs_results[dev_i].dev_types):
                if dev_type_el[0] == dev_type:
                    worksheet.write(26+dev_i, i + 1, dev_type_el[1])

        #chart_dev_type_comp.add_series({
        #    'name': logs_results[dev_i].name,
        #    'categories': [sheet_name, 25, 1, 25, len(dev_type_cmp)],
        #    'values': [sheet_name, 26 + dev_i, 1, 26 + dev_i, len(dev_type_cmp)],
        #})

        worksheet.write(50, 0, 'Manufacturers ID:', format1)
        worksheet.write(51 + dev_i, 0, logs_results[dev_i].name)
        for i, manu_id in enumerate(manu_id_cmp):
            worksheet.write(50, i + 1, manu_id, format1)
            for j, manu_id_el in enumerate(logs_results[dev_i].manu_ids):
                if manu_id_el[0] == manu_id:
                    worksheet.write(51 + dev_i, i + 1, manu_id_el[1])
        #chart_manu_id_comp.add_series({
        #    'name': logs_results[dev_i].name,
        #    'categories': [sheet_name, 50, 1, 50, len(manu_id_cmp)],
        #    'values': [sheet_name, 51 + dev_i, 1, 51 + dev_i, len(manu_id_cmp)],
        #})

        worksheet.write(75, 0, 'SN:', format1)
        worksheet.write(76 + dev_i, 0, logs_results[dev_i].name)
        for i, frames_from_SN_iter in enumerate(frames_from_SN_cmp):
            worksheet.write(75, i + 1, frames_from_SN_iter, format1)
            for j, frames_from_SN_el in enumerate(logs_results[dev_i].frames_from_SN):
                if frames_from_SN_el[0] == frames_from_SN_iter:
                    worksheet.write(76 + dev_i, i + 1, frames_from_SN_el[1])
        #chart_frames_from_SN_comp.add_series({
        #    'name': logs_results[dev_i].name,
        #    'categories': [sheet_name, 75, 1, 76, len(frames_from_SN_cmp)],
        #    'values': [sheet_name, 76 + dev_i, 1, 76 + dev_i, len(frames_from_SN_cmp)]
        #})

    #worksheet.insert_chart('C33', chart_dev_type_comp)
    #worksheet.insert_chart('C10', chart_rssi_comp)
    #worksheet.insert_chart('C58', chart_manu_id_comp)
    #worksheet.insert_chart('C83', chart_frames_from_SN_comp)

    workbook.close()
    print('Saving to xlsx file completed.')


def readHexLogsFromFile(input_file):
    with open(input_file, 'r+') as file:
        string_logs = file.read()
        hex_logs = bytearray.fromhex(string_logs)
        file.close()
    return hex_logs


def readHexLogsFromS2LPFile(input_file):
    with open(input_file, 'r+') as file:
        string_logs = file.read()
        hex_logs = bytearray.fromhex(string_logs)
        file.close()
    return hex_logs


def createDDFrame(frame):
    m_frame = MaciejFrame(frame)
    if m_frame:
        wmbus_frame = WMBUST1Frame(m_frame.load)
        m_frame.remove_payload()
        dd_frame = m_frame/wmbus_frame
        #dd_frame.show()
        return dd_frame
    else:
        return None


def createDDTelitFrame(frame):
    wmbus_frame = WMBUST1Frame(frame)
    dd_frame = wmbus_frame
    return dd_frame


def createDDTelitRssiFrame(frame):
    wmbus_frame = WMBUST1Frame(frame)
    rssi_frame = Telit_RSSI(wmbus_frame.payload)
    rssi_frame.RSSI = -rssi_frame.RSSI
    wmbus_frame.remove_payload()
    dd_frame = wmbus_frame/rssi_frame
    return dd_frame


def frames_from_SN_distribution(frames):
    SNs = [frame.Address for frame in frames]
    SNs_hex_str = [bytes(SN).hex() for SN in SNs]
    frames_from_SN = Counter(SNs_hex_str)
    frames_from_SN.most_common()
    frames_from_SN = sorted(frames_from_SN.items(), key=lambda column: column[1], reverse=True)
    return frames_from_SN


def RSSI_distribution(frames):
    rssi = [frame.RSSI for frame in frames]
    rssi_counted = Counter(rssi)
    rssi_counted.most_common()
    rssi_counted = sorted(rssi_counted.items())
    return rssi_counted


def device_type_distribution(frames):
    device_type = [str_dtype_from_byte(frame.Device_type) for frame in frames]
    device_type_counted = Counter(device_type)
    device_type_counted.most_common()
    device_type_counted = sorted(device_type_counted.items(), key=lambda column: column[1])
    return device_type_counted


def manufacturers_id_distribution(frames):
    manu_id = [frame.ManuId_field for frame in frames]
    manu_id_string = [str_mid_from_bytes(mid) for mid in manu_id]
    manu_id_counted = Counter(manu_id_string)
    manu_id_counted.most_common()
    manu_id_counted = sorted(manu_id_counted.items(),  key=lambda column: column[1])
    return manu_id_counted


def unique_SN_distribution(frames):
    SN = [bytes(frame.Address).hex() for frame in frames]
    SN_counted = Counter(SN)
    unique_SN_count = len(SN_counted)
    return unique_SN_count


def str_mid_from_bytes(bytes):
    if bytes:
        ManuIDdec = bytes[1]*16*16 + bytes[0]
        first_letter = int(ManuIDdec/1024)
        rest = ManuIDdec%1024
        second_letter = int(rest/32)
        rest = rest%32
        third_letter = int(rest)
        first_letter = first_letter + 64
        second_letter = second_letter + 64
        third_letter = third_letter + 64
        manu_id_str = ''.join(chr(i) for i in [first_letter, second_letter, third_letter])
        return manu_id_str
    else:
        return 'BAD'


def str_dtype_from_byte(byte):
    if byte > 0x19:
        dev_type_str = "Other"#str(byte)
    else:
        dev_type_list = [(0x00, "Other"),
                        (0x01, "Oil"),
                        (0x02, "Electricity"),
                        (0x03, "Gas"),
                        (0x04, "Heat out"),
                        (0x05, "Stream"),
                        (0x06, "HotWater"),
                        (0x07, "Water"),
                        (0x08, "Heat Cost Allocator."),
                        (0x09, "Compressed Air"),
                        (0x0A, "Cooling load meter out"),
                        (0x0B, "Cooling load meter in"),
                        (0x0C, "Heat in"),
                        (0x0D, "Heat/Cooling load meter"),
                        (0x0E, "Bus/System"),
                        (0x0F, "Other"),
                        (0x10, "Other"),
                        (0x11, "Other"),
                        (0x12, "Other"),
                        (0x13, "Other"),
                        (0x14, "Other"),
                        (0x15, "Other"),
                        (0x16, "Cold Water"),
                        (0x17, "Dual Water"),
                        (0x18, "Pressure"),
                        (0x19, "A/D Converter")]
        dev_type_str = dev_type_list[byte][1]
    return dev_type_str


if __name__ == "__main__":
    logger(sys.argv[1:])
