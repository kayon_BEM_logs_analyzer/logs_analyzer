from scapy.all import *

class WMBUST1Frame(Packet):
    name = "WMBUS"
    fields_desc=[ByteField("Length_field", 0x0A),
                 ByteEnumField("Control_field", 0x44, {0x44: "Send/No Reply"}),
                 FieldListField("ManuId_field", None, XByteField("StartByte", None), count_from=lambda pkt: 2),
                 FieldListField("Address", None, XByteField("StartByte", None), count_from=lambda pkt: 4),
                 XByteField("Version", 0x00),
                 ByteEnumField("Device_type", 0x00, {0x00: "Other",
                                                     0x01: "Oil",
                                                     0x02: "Electricity",
                                                     0x03: "Gas",
                                                     0x04: "Heat out",
                                                     0x05: "Stream",
                                                     0x06: "HotWater",
                                                     0x07: "Water",
                                                     0x08: "Heat Cost Allocator.",
                                                     0x09: "Compressed Air",
                                                     0x0A: "Cooling load meter out",
                                                     0x0B: "Cooling load meter in",
                                                     0x0C: "Heat in",
                                                     0x0D: "Heat/Cooling load meter",
                                                     0x0E: "Bus/System",
                                                     0x0F: "Unknown Medium",
                                                     0x16: "Cold Water",
                                                     0x17: "Dual Water",
                                                     0x18: "Pressure",
                                                     0x19: "A/D Converter"}),
                 FieldListField("Encrypted_block", None, XByteField("StartByte", None), count_from=lambda pkt: pkt.Length_field-9),

                 ]
