from scapy.all import *

class MaciejFrame(Packet):
    name = "Maciej Frame"
    fields_desc = [FieldListField("StartBytes", None, XByteField("StartByte", None), count_from=lambda pkt:4),
                   ByteField("RSSI", 0)]
