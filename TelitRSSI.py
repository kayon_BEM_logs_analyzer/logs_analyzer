from scapy.all import *


class Telit_RSSI(Packet):
    name = "TELIT_RSSI"
    fields_desc = [SignedByteField("RSSI", 0x00)]